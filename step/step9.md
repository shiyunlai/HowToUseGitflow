
Step 9：发布测试通过，申请进入预发布
---

|||
|:---------:|--------------------|
|何时做|发布计划中的内容通过了测试，申请进入预发布（beta版本）。|
|谁来做|功能Owner|
|有限制|通过PR进行申请 |
**如何做：**

* 同步分支
    
    ```bash
    # 同步integrate分支
    $ git checkout integrate 
    $ git pull 
  
    # 同步release分支
    $ git checkout release_V1.5.0-bike 
    $ git pull 
    ```
    
* 预合并integrate
    
    ```bash
    $ git checkout integrate
    $ git merge --no-ff release_V1.5.0-bike
    ```

* 申请Pull Request合并到integrate
    
      * PR要求(本列使用gitee)
      
          * 源分支 [release_V1.5.0-bike]
          * 目标分支 [integrate]
          * 标题 [预发布版本V1.5.0-bike]
          * 审查人员 [库管理员/迭代开发负责人]
          * 必须审查代码 [true]
          
     ![Pull Request integrate](img/beta-integrate.png)
    
* 预合并master

    ```bash
    $ git checkout master
    $ git merge --no-ff release_V1.5.0-bike
    ```

* 申请Pull Request合并到master

    * PR要求(本列使用gitee)
        * 源分支 [release_V1.5.0-bike]
        * 目标分支 [master]
        * 标题 [预发布版本V1.5.0-bike]
        * 审查人员 [库管理员/迭代开发负责人]
        * 必须审查代码 [true]
        
    ![Pull Request integrate](img/beta-master.png)
