
###step1

<span id="anchor"><font color="red">锚点</font></span>

|||
|:---------:|--------------------|
|何时做|启动一个新的产品/项目开发，init仓库、创建代码工程。|
|谁来做|研发经理/架构师|
|有限制|固定命名integrate，用作功能集成。|
**如何做：**

* 初始化仓库，创建工程、代码、文档等。

    ```bash
    $ git init
    ```

    增加各种工程、代码、配置、文件...

* 提交到中央仓库（以gitee为列。也可用github，或自建gitlib）

    登录Gitee，新建一个项目，得到clone地址。如： https://gitee.com/shiyunlai/HowToGitflow.git

    将本地库与中心仓库关联
    ```bash
    $ git remote add origin https://gitee.com/shiyunlai/HowToGitflow.git
    ```

    推送内容到中心仓库
    ```bash
    $ git push -u origin master
    ```

* 创建集成分支（integrate）

    从master创建集成分支integrate
    ```bash
    $ git checkout master
    $ git checkout -b integrate
    ```

    将集成分支推送到中心仓库
    ```bash
    $ git checkout integrate
    $ git push origin integrate
    ```

* 至此，形成master、integrate两条主分支，构成了gitflow的初始模型。

    本地仓库分支状态：
    ![本地分支状态](../img/init.png)

    远程中心仓库分支（gitee）状态：
    ![gitee分支状态](../img/init-gitee.png)

    >注意：master,integrate设置为只读分支(gitee功能)，防止错误提交。

    >另外：以上操作，可直接在gitee上操作完成。
