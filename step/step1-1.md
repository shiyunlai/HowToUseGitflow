
###step1-1

<span id="anchor"><font color="red">锚点</font></span>

|||
|:---------:|--------------------|
|何时做|本地已经有一个工程，想共享到远程仓库|
|谁来做|研发经理/架构师|
|有限制|已经创建好了工程目录/或者直接已经造好一个目录结构， 如： Develop/spdemo|
**如何做：**

1、（先进入项目文件夹）通过命令 git init 把这个目录变成git可以管理的仓库

```bash
$ git init
```
2、把文件添加到版本库中，使用命令 git add .添加到暂存区里面去，不要忘记后面的小数点“.”，意为添加文件夹下的所有文件

```bash
$ git add .
```

3、用命令 git commit告诉Git，把文件提交到仓库。引号内为提交说明

```bash
$ git commit -m 'first commit'
```

4、关联到远程库

```bash
$ git remote add origin 你的远程库地址
```
如：
```bash
$ git remote add origin https://gitee.com/shiyunlai/spring-cloud-demo.git
```

如果要替换远程库地址(删除后重新设置) 
```bash
$ git remote rm origin
$ git remote add origin 新的远程库地址
```

此处，假设已经在gitee或者gitlib、github之类的远程仓库创建了工程。

5、获取远程库与本地同步合并（如果远程库不为空必须做这一步，否则后面的提交会失败）

```bash
$ git pull --rebase origin master
```

rebase的效果见下图：

![rebase的作用](img/whatisrebase.png)

[更多资料见,关于Git超赞的讲解](https://www.jianshu.com/p/d4d5e07ea0cd?utm_campaign=hugo&utm_medium=reader_share&utm_content=note&utm_source=weixin-friends "关于Git超赞的讲解")
 
>通俗的解释就是new分支想站在dev的肩膀上继续下去。rebase也需要手动解决冲突。

6、把本地库的内容推送到远程，使用 git push命令，实际上是把当前分支master推送到远程。执行此命令后会要求输入用户名、密码，验证通过后即开始上传。

```bash
$ git push -u origin master
```

最后查看一下状态
```bash
$ git status
```

