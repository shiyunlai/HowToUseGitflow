
Step 8：发布阶段修复BUG]
---

|||
|:---------:|--------------------|
|何时做|对发布内容测试，发现有BUG，解决这些BUG|
|谁来做|开发人员|
|好建议|直接在release分支上解决BUG |
|     |每解决掉一个BUG，打一个Tag，标记一下 |
|有限制||
**如何做：**

    重复step 4、step 5的操作（把release当作一个特殊的feature看待）；

* 获取release分支

    见[step 4]

* 在release上修复bug
    
    见[step 5]
    
    >注意：修复的内容第一时间合并推送回中心库release分支
