Step 5：加入功能开发,完成功能开发]
---

|||
|:---------:|--------------------|
|何时做|开发过程中 —— 同步代码、写代码、提交代码、版本回退、分支合并等等的时候
|谁来做|开发人员开发人员，如：小A，小B，小C三人合作，正在开发高级清单自行车|
|有限制|这些操作，只在feature分支上发生|
**如何做：**

* 先同步feature代码
    
    ```bash
    $ git checkout feature_senior_bike
    $ git pull
    ```
    
* 新建[任务分支]，隔离并发多个任务一起开发（不强制.但,强烈建议按任务建本地开发分支!）

    新建任务分支，另见[任务分支命名规范]，如：工作项360这个功能开发任务 task_senior_bike_workitem360
    ```bash
    $ git checkout feature_senior_bike
    $ git checkout -b task_senior_bike_workitem360
    ```
    
    推送自己的[任务分支]到中心库（不强制！当天不能完工想借助中心库保护/共享源码）
    
    ```bash
    $ git push origin task_senior_bike_workitem360
    ```
    
* 编码过程中（常用操作）
    
    切换到[任务分支]
    ```bash
    $ git checkout  task_senior_bike_workitem360
    ``` 
    
    编写代码...完成了一点，或准备一次阶段提交
     
    提交代码，另见[注释规范]
    ```bash
    $ git commit -a -m "完成xxx"
    ```
    
    "git commit -a -m ... " 等同于
    
    先保存到stash
    ```bash
    $ git add .
    ```
    
    然后再commit
    ```bash
    $ git commit -m "注释内容..."
    ```
    
    版本回退: 回到从前（时光穿梭）
    ```bash
    # 为了知道回滚位置，先用 git log 查看之前的提交日志，包括 commit id 和 commit comment
    $ git log --pretty=oneline
  
    # 然后回滚到指定的提交ID
    # 通过指定commit id 可以回滚到对应的版本
    $ git reset --hard [commit id]
    ```
    
    版本回退：后悔了，不想回退了（时光穿梭）
    ```bash
    # 回滚错了，先用 git reflog 查看所有提交（包括回滚代码时被删除的commit id记录）
    # git reflog 查看本地会影响HEAD指针的命令操作记录，这个不会同步到远程仓库
    $ git reflog 
  
    # 然后找到回滚点，回滚
    $ git reset --hard [commit id]
    ```
    
    版本回退：也可以这么操作，回退到前n个版本
    ```bash
    # 回退到上一个版本
    git reset --hard HEAD^
    
    # 或指定回滚到前n个版本
    $ git reset --hard HEAD~n
    ```
    
* 合并[任务分支]到[feature分支]

    重复常用操作，直到任务开发完成，可以提交 
    ```bash
    # 先更新feature
    $ git checkout feature_senior_bike
    $ git pull
    
    # 合并任务分支
    $ git merge --no-ff task_senior_bike_workitem360
    
    # 有冲突，则处理掉（可借助各种工具辅助）
    $ ...
  
    # 无冲突，则记录注释(如：完成任务workitem360)，提交代码
    
    # 标记本次提交（这个不用推送到中心库）
    $ git tag -a B_done_task_senior_bike_workitem360 -m "完成xxx开发"
    
    # 推送feature到中央仓库（别人 git pull 时将拿到你刚刚提交的代码）
    $ git push origin feature_senior_bike
    ```
    
    另见[解决冲突]
    
* 重复[新建任务分支] -> [常规操作] -> [合并]， 完成更多任务的开发
