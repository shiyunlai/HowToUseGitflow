
Step 2：拉代码
---

|||
|:---------:|--------------------|
|何时做|开发人员本地还没有这些工程（如：小B，新加入团队。）|
|谁来做|新加入的人员（如：开发人员、测试人员、架构师...）|
|有限制|无|
**如何做：**

* clone工程到本地

    建工作空间
    ```bash
    $ mkdir workspace
    $ cd workspace
    
    # clone工程
    $ git clone https://gitee.com/shiyunlai/HowToGitflow.git 
    $ cd HowToGitflow
    ```
    
* 切换到集成分支

    ```bash
    # 下载并切换到 integrate
    $ git checkout -b integrate origin/integrate
  
    # 查看一下分支状态 已经切换在 * integrate 分支了 
    $ git branch
    ```
