
Step 7：启动发布计划]
---

|||
|:---------:|--------------------|
|何时做|integrate收到多个完工的feature，满足发布计划(release)，开始一次发布计划|
|谁来做|测试Leader/功能Owner/架构师/研发经理|
|有限制|为每次发布计划指定一个版本,另见[版本规则];|
|     |从integrate创建release分支;|
|     |一旦创建发布分支，就进入封闭，其他功能等下一次发版窗口，有bug直接在发布分支修复;|
**如何做：**
    
    假设
    本次发布计划的版本号为：V1.5.0-bike
    这是alpha版本

* 先同步integrate分支
    
    ```bash
    $ git checkout integrate
    $ git pull 
    ``` 

* 在integrate上打标签，标记开始这次发布计划
    
    ```bash
    # 新建标签
    $ git tag -a before_V1.5.0-bike -m "启动V1.5.0-bike发布计划前"
    
    # 把tag推送会中心库
    $ git push origin before_V1.5.0-bike
    ``` 

* 创建发布计划realease分支
    
    ```bash
    # 新建elease_V1.5.0-bike
    $ git checkout -b release_V1.5.0-bike
    
    # 推送回中心仓库
    $ git push origin release_V1.5.0-bike
    ``` 
   