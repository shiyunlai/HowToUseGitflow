
Step 4：新建功能分支
---

|||
|:---------:|--------------------|
|何时做|开始功能开发|
|谁来做|开发人员|
|有限制|无|
**如何做：**

* 同步本地库（非必要）

    ```bash
    $ git checkout master
    $ git pull
    $ git checkout integrte
    $ git pull
    ``` 
    
* 获取开发分支

    检查是否已经有目标分支，如：feature_senior_bike
    ```bash
    $ git fetch origin feature_senior_bike 
    ```
    
    报错：....，说明分支还没建好
    ```bash
    fatal: Couldnt find remote ref feature_senior_bike
    ```
     
    不报错
    ```bash
    From https://gitee.com/shiyunlai/HowToGitflow
     * branch            feature_senior_bike -> FETCH_HEAD
    ```
    
    继续获取分支
    ```bash
    # 将远程feature_senior_bike拉到本地，并checkout到feature_senior_bike
    $ git checkout -b  feature_senior_bike origin/feature_senior_bike
    ```
    
    获取分支后，建议先做个Tag，方便以后回滚
    
    ``` bash
    $ git tag  -a B_join_feature_senior_bike_[yyyyMMddHHmm] -m '加入'
    ```
    
    > 注意： 个人标签无需推送到远程中心库
    
* 至此，小B完成了加入开发，可以开始各种骚编码了！
