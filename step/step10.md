Step 10：正式发布某个版本]
---

|||
|:---------:|--------------------|
|何时做|正式发布某个版本|
|谁来做|研发经理/架构师|
|好建议|发版后，一定要记得打Tag，标记一下 |
|有限制||
**如何做：**

* 研发经理通过某次预发布PR申请

* 进行标记

    ```bash
    $ git checkout master
    $ git pull
    $ git tag -a V1.5.0-bike -m "发布新版本：V 1.5.0-bike" 
    $ git push origin  V1.5.0-bike
    ```
    
    更多操作，另见[Tag操作](#Tag操作 "")
    
* 删除release分支

    ```bash
    # 删除本地
    $ git branch -D release_V1.5.0-bike
    
    # 删除远程
    $ git push origin --delete  release_V1.5.0-bike
    ```
    
    更多操作，另见[分支操作](#分支操作 "")
