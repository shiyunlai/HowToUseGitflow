Step 11：修复生产问题]
---

|||
|:---------:|--------------------|
|何时做|生产环境发现问题，进行修复|
|谁来做|开发人员|
|有限制|hotfix分支从master直接创建；|
**如何做：**

* 同步master分支

    ```bash
    $ git checkout master
    $ git pull 
    ```
    
* 新建hotfix分支，如： BUG编号为 789

    ```bash
    $ git checkout -b hotfix_789
    ```
    
* 在hotfix_789上修复问题
    
    见[常用操作]
    
* 问题修复，进行发布
    
    ```bash
    # 更新master
    $ git checkout master 
    $ git pull
  
    # 预合并，提前解决冲突
    $ git merge --no-ff hotfix_777
  
    # 将hotfix推送到中心库
    $ git push origin hotfix_777
    ``` 

* 申请Pull Request合并到master

    * PR要求(本列使用gitee)
        * 源分支 [hotfix_777]
        * 目标分支 [master]
        * 标题 [修复bug789，解决xxxx问题]
        * 审查人员 [库管理员/研发经理/架构师]
        * 必须审查代码 [true]

    ![修复BUG，发布新版本](img/hotfix-master.png)

* 通过审核，接受PR，完成bug投产

* 为master打Tag，标记这次bug修复
 
    ```bash
    # 先同步master
    $ git checkout master
    $ git pull
    
    # 打标签，且小版本号+1
    $ git tag -a V1.5.1-bike -m "done bug 789，解决xxxx问题" 
    $ git push origin V1.5.1-bike 
    ```
    
    建议在gitee上直接完成Tag
    ![gitee 进行tag](img/ad.png)
    
* 申请Pull Request合并回integrate

    * PR要求(本列使用gitee)
        * 源分支 [hotfix_789]
        * 目标分支 [integrate]
        * 标题 [修复bug789，解决xxxx问题]
        * 审查人员 [库管理员/研发经理/架构师]
        * 必须审查代码 [true]
        
    ![修复BUG，发布新版本](img/hotfix-integrate.png)
    
* 通过审核，接受PR，bug的修复代码回到integrate分支

* 为integrate打Tag，标记这次bug修复
 
    ```bash
    # 先同步integrate
    $ git checkout integrate
    $ git pull
    
    # 打标签，且小版本号+1
    $ git tag -a fixed_bug780 -m "done bug 789，解决xxxx问题" 
    ``` 
    
    也建议在Gitee上操作 
    
* 删除hostfix
 
     ```bash
     $ git branch -d hotfix_789
     
     $ git push origin --delete  hotfix_789
     ```

至此，单周期的过程列举完毕！
