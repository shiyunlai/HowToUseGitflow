Step 3：启动迭代计划
---

|||
|:---------:|--------------------|
|何时做|启动某次迭代开发，如：高级清单系统，自行车计划的第一次迭代开发，代号：bike|
|谁来做|功能Owner|
|有限制| 从[integrate分支]新建[feature分支];|
|     |feature分支命名规范：feature_[应用系统]_[代号|计划描述|特性描述]|
|     |feature分支命名规范，另见[feature分支命名](#feature分支命名)|
**如何做：**

* 新建功能开发分支

    先同步integrate分支
    ```bash
    $ git checkout integrate
    $ git pull
    ```
    
    建功能分支
    ```bash
    # 高级清单自行车开发专用分支 feature_senior_bike
    $ git checkout -b feature_senior_bike
    ```
    
* 将feature分支推回中心库（方便开发小组协作）

    ```bash
    $ git push origin feature_senior_bike
    ```

* 至此，高级清单自行车计划第一次迭代开发的分支建好:feature_senior_bike。参加开发的成员，可以拉取代码开始编码。
    本地
    ![integrateok后本地分支状态](img/integrate-ok.png)
    
    远程
    ![integrateok后远程分支状态](img/integrate-ok-gitee.png)
    