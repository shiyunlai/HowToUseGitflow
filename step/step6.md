
Step 6：交付功能]
---

|||
|:---------:|--------------------|
|何时做|feature完工，交付integrate进行集成|
|谁来做|参与该功能开发的所有人均可|
|好建议|由功能Owner操作|
|     |在本地更新integrate后，先进行一次merge，解决掉冲突，再推送远程，申请PR。|
|有限制|借助中心仓库，以PR的方式交付|
**如何做：**

* Owner先更新feature，取得最新最全功能代码(确保开发成员已提交所有代码到feature)
    
    ```bash
    $ git checkout feature_senior_bike
    $ git pull 
    ```

* 更新integrate分支
    
    ```bash
    $ git checkout integrate
    $ git pull
    ```
    
* 预合并（提前解决与integrate的冲突）
    
    ```bash
    $ git checkout integrate
    $ git merge --no-ff feature_senior_bike 
    ```
    
    > 注意：integrate分支始终以中心库为准，本地的提交不到中心库（integrate为只读分支）。
    
    > 强行git push，将报错
    
    ![integrate is a readonly banch](img/integrate-readonly.png)
    
* 如果提示有冲突，则逐一解决
    
    另见[解决冲突]
    
* 如果没有冲突，则推送feature到远程
    
    ```bash
    $ git checkout feature_senior_bike
    $ git push origin feature_senior_bike 
    ```
    
* 发起Pull Request
    
    * PR要求(本列使用gitee)
    
        * 源分支 [feature_senior_bike]
        * 目标分支 [integrate]
        * 标题 [交付XXX功能]
        * 审查人员 [库管理员/迭代开发负责人]
        * 必须审查代码 [true]
        
    如下图：     
    ![delivery feature](img/delivery-feature.png)
    
* 库管理员在gitee上，完成PR接受

* 如接收PR时，有冲突

    另见[PR冲突解决]

* PR被接收后，可删除feature分支

    ```bash
    # 删除本地分支 
    # 切换到integrate
    $ git checkout integrate
    # 删除feature_senior_bike
    $ git branch -D feature_senior_bike
  
    # 再删除远程分支 
    $ git push origin --delete  feature_senior_bike
    ```
    
    > 注意：删除远程分支后，不影响其他成员本地的分支，除非本人在本地执行删除命令。
    