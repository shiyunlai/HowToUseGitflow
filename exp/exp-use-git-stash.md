# Git stash 的使用

Git Stash 

git stash用于将当前工作区的修改暂存起来，就像堆栈一样，可以随时将某一次缓存的修改再重新应用到当前工作区。

## 使用场景1： 
    
    正在开发，修改了一些文件，突然有其他工作需要先解决 —— 如：生产bug

## 使用场景2:
    
    修改完一段代码，之后感觉有新的方案会更好， 可把写好的代码stash，然后实验第二中方案；


### 方案1 ： 新拉一份代码，新建一个工作空间 —— 物理上分开，单独解决这个生产bug； 

### 方案2 ： 使用 git stash

    0、当前工作分支curr_dev，修改了一些代码
    1、 保存修改过的文件到 stash 区，每次暂存有一个名称，如：stash_1 （注意不是提交，只是把变化保存在暂存区）
    2、 切回生产分支
    3、 同步生产分支
    4、 新建bug修复分支 bugfix_xxxx
    5、 修复问题，提交，完事
    6、 切回curr_dev
    7、 恢复名称为 stash_1的暂存内容
    8、 代码工程恢复到 0 时的状态

 	实现：用git命令（ 也可使用idea （Git 》Repository 》 Stash Changes… | Unstash Changes ….） ）
                
             # 暂存当前分支上的修改，并为stash记录取名为 stash_dev_1
             git stash save “stash_dev_1”
             
             # 2，3，4，5，6 步骤的命令省略了
    
             # 切换curr_dev分支
             git checkout curr_dev
             
             # 查看一下有哪些stash记录
             git stash list
            
             # 取回stash_dev_1内容，合入当前分支
             git stash apply “stash_dev_1”

### Git Stash 命令总结

* stash当前修改，并备注信息为 stash_1， git会为每个stash自动分配唯一ID，供后续操作时使用，如： stash@{0}
    ```shell
    $ git stash save 
    ```

*  将缓存堆栈中的第一个stash删除，并将对应修改应用到当前的工作目录下
    ```shell
    $ git stash pop 
    ```
*  将缓存堆栈中第一个stash应用到工作目录中，但并不删除stash拷贝（于是可以多次使用这个暂存内容）
    ```shell
    $ git stash apply
    ```
* 查看有哪些缓存记录 
    ```shell
    $ git stash list
    ```
* 通过名称指定应用那个暂存记录，这里 stash@{0}是暂存记录的ID
    ```shell
    $ git stash apply stash@{0}
    ```
* 移除缓存记录
    ```shell
    $ git stash drop 暂存记录ID 
    ```
* 删除所有缓存记录
    ```shell
    $ git stash clear
    ```
* 查看指定缓存记录中有修改的文件，加 -p 或 - -patch 可查看具体的diff内容
    ```shell
    $ git stash show 暂存记录ID
    $ git stash show -p 暂存记录ID
    $ git stash show --patch 暂存记录ID
    ```
    
* 从缓存记录上创建分支（捷径，实现从上一次修改内容继续修改）
    ```shell
    $ git stash branch 分支名称
    ```
* 暂存未跟踪的文件（被 .gitignore 忽略掉的内容也可以暂存）
    
    - 暂存时，包括所有untracked files
    ```shell
    $ git stash save -u 暂存记录ID
    $ git stash save --include-untracked 暂存记录ID
    ```
    - 暂存时，包括所文件
    ```shell
    $ git stash save -a 暂存记录ID
    $ git stash save -all 暂存记录ID
    ```
    
    >说明：默认情况下，git stash会缓存下列文件
 	>- 添加到暂存区的修改（staged changes）
 	>- Git跟踪的但并未添加到暂存区的修改（unstaged changes）
    >
    >  但不会缓存一下文件：
 	>- 在工作目录中新的文件（untracked files）
 	>- 被忽略的文件（ignored files）

### 使用Git Stash 的一些注意事项

- 1、stash是本地的，不会通过git push命令上传到git server上；
- 2、git会为每个stash记录分配一个ID，通过该ID指定对应的缓存记录；

