
# Gitflow开发协作流程

**拥抱git，用正确的方法，进行开发协作！**

![[ttt-gitflow]][png-gitflow]

这种开发分支管理可以叫做【功能点交付策略】，他是以功能点交付为开发管理目标，重点在于对功能点的独立开发管理，如：当前开发团队总共有10个功能点已经完工，但是由于业务目标变更，等非可控因素，要求只投产其中相关的6个，其他4个下一批次投产；也有可能6个功能都已经测试妥当，这时突然又要再加入1个；总之功能点交付策略重点在于对功能点的解耦，可快速应对功能点切分、合并、延期投产等业务要求；

功能点交付策略管理模式，不要求有明确的迭代规划，随时都在开发，随时都在投产 —— 迭代火车交付火车，没精力搞迭代计划，来功能点，干就完来！

另外还有一种 [【迭代继承策略】][masterdevelop]，相对这种比较简单，主要适用于有明确迭代规划的开发行为，迭代时间明确、迭代期内工作内容明确，交付内容明确...这种模式不多讲！

> - 假设：
> 	- 配图中正在进行代号为"**ROCK**"的迭代开发；
> 	- V1.5.0版本准备发布的功能包括："ROCK"和"X"两个特性"；
> - 上图副本见[processon][ttt-gitflow]

## 简述

- 需借助中心仓库，如：gitee、github、自建gitlib均可；
- 本gitflow模型中，共有6种分支类型；
- master、integrate为常驻分支，建议设置为"只读分支"；
- Task分支只在开发人员本地，其他5种分支需要上中心库；
- release通过发版测试后，合并到master，从master上制作发布包；
- 

## 分支规则

gitflow流程中一共有6中分支，每种分支的规则如下：

- [master 分支][master]
- [integrate 分支][integrate]
- [release 分支][release]
- [hotfix 分支][hotfix]
- [feature 分支][feature]
- [task 分支][task]

## 照着做 ( Work on Gitflow )

* [Step 1：建库][step1]
* [Step 1-1：已经新建好工程推送到远程仓库][step1-1]
* [Step 2：拉代码][step2]
* [Step 3：启动迭代计划][step3]
* [Step 4：新建功能分支][step4]
* [Step 5：加入功能开发,完成功能开发][step5]
* [Step 6：交付功能][step6] 
* [Step 7：启动发布计划][step7]
* [Step 8：发布阶段修复BUG][step8]
* [Step 9：发布测试通过，申请进入预发布][step9]
* [Step 10：正式发布某个版本][step10]
* [Step 11：修复生产问题][step11]

> 对TTT产品的研发来说，每个迭代开发计划，都是step 3 ~ step 10这些过程的周而复始。

## FQA

* [FAQ 1：分支规范，如：命名、用途、基本操作等][faq-branch]
* [FQA 2：版本号规范][faq-version]
* [FQA 3：Tag操作][faq-tag]
* [FQA 3：如何解决代码冲突][faq-conflict]
* [FQA 4：更换远程仓库地址][faq-chg-remote]

## Git使用经验 

* [exp 1：使用git stash][exp-use-git-stash]

## 参考资料

- [Gitflow工作流][ref1]

-------

[ttt-gitflow]:https://www.processon.com/view/link/5b88def5e4b0fe81b6200454
[png-gitflow]:img/gitflow.png

[ref1]:https://www.cnblogs.com/jiuyi/p/7690615.html

[faq-branch]:faq/faq-分支规范.md
[faq-version]:faq/faq-版本号规范.md
[faq-tag]:faq/faq-标签规范.md
[faq-conflict]:faq/faq-解决冲突.md
[faq-chg-remote]:faq/faq-更换远程仓库.md

[exp-use-git-stash]:exp/exp-use-git-stash.md

[step1]:step/step1.md
[step1-1]:step/step1-1.md
[step2]:step/step2.md
[step3]:step/step3.md
[step4]:step/step4.md
[step5]:step/step5.md
[step6]:step/step6.md
[step7]:step/step7.md
[step8]:step/step8.md
[step9]:step/step9.md
[step10]:step/step10.md
[step11]:step/step11.md

[master]:branch/master.md
[integrate]:branch/integrate.md
[release]:branch/release.md
[hotfix]:branch/hotfix.md
[feature]:branch/feature.md
[task]:branch/task.md

[masterdevelop]:masterdevelop/masterdevelop.md

