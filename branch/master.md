# master 分支

> 分支类型
* 常规分支：项目成员（开发者权限及以上）可Push分支
* 保护分支：项目管理员才能管理（Push）被保护的分支。
* 只读分支：任何人都无法Push代码（包括管理员和所有者），需要Push代码时应设为“常规”或“保护”分支。

> 规则

始终仅有的一个master分支；
用于发布最终产品——在master上制作投产包；（与生产环境代码同步）
gitee设置为只读分支；（不允许直接提交代码到master）
为master分支上的提交分配一个版本号；

由管理员创建；（项目初始化时就有）

与其他分支的关系：
可接收release分支的pull request；（可以直接合并）
可接收单个hotfix分支的pull request；（可以直接合并）
可接收issues分支的pull request；（可以直接合并）
每次接收release分支的pull request后，要做tag；    （tag规范： tag-版本号，同时tag注释说明发版功能概要）
每次接收单个hotfix分支的pull request后，要做tag；（tag规范： tag-版本号，小版本+1，同时tag注释说明hotfix）
每次接收单个issues分支的pull request后，要做tag；（tag规范： tag-版本号，中版本+1，同时tag注释说明issue了那些hotfix）

永不删除；