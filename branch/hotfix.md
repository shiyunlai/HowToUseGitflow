同时会有多个hotfix分支，每个生产问题issue，新建一个hotfix分支；（只有生产问题才创建hotfix分支）
用于修复生产问题；
gitee设置为常规分支；（修复生产问题时，提交代码到hotfix分支）

由开发人员创建；

与其他分支的关系：
创建自master分支；
不接收任何pull reques；
问题修复后，pull request到master分支；（可以直接合并）
问题修复后，pull request到develop分支；（可能存在代码冲突）

修复完问题，投产后，即可删除；

特殊情况：如果同时有多个bug投产，建议新建一个issues分支，进行多个hotfix的集成合并测试，之后再整体pull reques到master和develop；

同时会有多个issues分支，如果同时投产1+个hotfix则新建issues分支；
用于同时投产1+个生产问题；
gitee设置为保护分支；

由管理员/开发人员创建；

与其他分支的关系：
创建自master分支；
只接收hotfix的pull reques；（可能存在代码冲突）
问题集中投产后，pull request到master分支；（可以直接合并）
问题集中投产后，pull request到develop分支；（可能存在代码冲突）

修复完问题，投产后，即可删除；