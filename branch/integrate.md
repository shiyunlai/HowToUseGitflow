同时仅有的一个develop分支；
用于集成开发过程中的代码；（代码多于master分支，包括开发好提交的代码）
gitee设置为只读分支；（不允许直接提交代码到develop）

由管理员创建；

与其他分支关系：
创建自master分支；
可接收feature分支的pull request；（可能存在代码冲突。开发中的功能，集中到develop准备发版测试）
可接收hotfix分支的pull request；（可能存在代码冲突。生产问题修复后，并入开发过程中）
可接收issues分支的pull request；（可能存在代码冲突。多个生产问题修复后，并入开发过程中）
每合并一个完成的功能，对develop进行tag；（tag规范： tag-feature名称，同时tag注释说明完工功能情况）

永不删除；

特色情况说明：