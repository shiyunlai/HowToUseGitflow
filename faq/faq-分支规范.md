# FAQ: 分支规范

> 分支类型

1. [主干 master](#主干（master）)
1. [修复分支 hotfix](#修复分支（hotfix）)
1. [集成分支 integrate](#集成分支（integrate）)
1. [功能分支 feature](#功能分支（feature）)
1. [任务分支 task](#任务分支（task）)
1. [发布分支 release](#发布分支（release）)

> 其他

* [常用分支操作命令](#常用分支操作命令)

## 主干（master）

## 修复分支（hotfix）

## 集成分支（integrate）

## 功能分支（feature）

>命名规则
1. 命名规则

    **feature_[*feature description words*]**
    
1. 以"feature_"开头，下划线分隔功能说明；
1. *feature description words*是功能说明，建议不超过3个单词；
   
    如：
    
    高级清单自行车功能开发分支：feature_senior_bike  
    
    完善ABF功能开发分支：feature_abf_grow     

## 任务分支（task）

>命名规则
1. 命名规则
    
    **task_[*task description words*]**
    
1. 以"task_"开头，下划线分隔任务说明；
1. *task description words*是任务描述，建议不超过4个单词；
1. 建议命名与来源feature有关联
   
    如：
    
    高级清单自行车功能中的工作项360开发分支：task_senior_bike_workitem360
    
    完善ABF功能中陈超(chenchao)负责后端API接口开发：task_abf_grow_chenchao


## 发布分支（release）

>命名规则
1. 命名规则
    
    **release_[*version NO*]_[*alias*]**
    
1. 以"release_"开头；
1. *version NO*是这次发布的版本号，每次发布都应该指定一个版本号，另见[版本号规范](faq/faq-版本号规范 "")；
1. *alias*是这次发布的别名，一般延用迭代计划所取的名称；
   
    如：
    
    发布高级清单系统第一次投产（内推）：release_V1.0.0_scooter

    发布高级清单自行车迭代一功能：release_V1.5.0_bike

## 常用分支操作命令

....