# 标签

- [对标签的理解](#对标签的理解)

- [标签命名规范](#命名规范)
    - [给master分支打标签](#给master打标签)
    - [给integrate分支打标签](#给integrate打标签)

- [标签操作命令](#标签操作命令)
    - [创建带附注的tag](#创建带附注的-tag-（推荐使用）) 
    - [快速创建tag（创建轻量级tag）](#创建轻量级-tag) 
    - [补打标签](#补打标签)
    - [查看有哪些标签，查看某个标签的信息](#查看标签列表，和详情)
    - [把标签推送（分享）到远程服务器](#把标签推送（分享）到远程服务器)
    - [把所有标签推送（分享）到远程服务器](#把所有标签推送（分享）到远程服务器)
    - [拉取tag内容（只能查看）](#拉取tag内容（只能查看）)
    - [根据tag创建一个分支（准备从tag处，开始修改内容）](#根据tag创建一个分支（准备从tag处，开始修改内容）)
    - [删除tag](#删除tag)
    - [删除远程仓库上的tag](#删除远程仓库上的tag)
    
    
## 对标签的理解

- 标签(tag)相当于是一个快照，所以，不能更改它的代码，要更改需 checkout -b 出来。

## 命名规范

### 给master打标签

1. 命名规范

    **[*version NO*]**

1. 直接用*version NO*版本号做Master分支的Tag标签名(每次发版都要标记)；

1. 举例

    ```bash
    # checkout到master分支
    git checkout master
    
    # 新建标签v1.5.0
    git tag -a v1.5.1 -m '投产bug789'
    ```

### 给integrate打标签

1. 命名规范
    
    **tag_[*tag description words*]**
    
1. 以"tag_"开头；

1. *tag description words*是标记描述，建议使用**状态动词开头**，不超过3个单词；
    
    如：
    
    完成高级清单自行车开发，交付integrate：tag_done_senior_bike
    
    完成操作日志功能开发，交付到integrate：tag_done_operatelog
    
    完成V1.5.1版本发布，回归到integrate：tag_deliveried_V1.5.1
    
    修改bug789后发布，回归到integrate：tag_fixed_bug789

1. 举例

    ```bash
    # checkout到integrate分支
    git checkout integrate
    
    # 新建标签v1.5.0
    git tag -a tag_fixed_bug789 -m '投产bug789,回归integrate'
    ```
    
## 标签操作命令

### 创建带附注的 Tag （推荐使用）

- 命令格式
    
    git tag -a [*标签名字*] -m [*标签说明*]

- 举例

    ```bash
    $ git tag -a v1.0.0 -m 'deliver V 1.0.0'
    ```

### 创建轻量级 Tag

- 命令格式

    git tag [标签名称]

- 举例

    ```bash
    $ git tag v1.0.0
    ```

### 补打标签
   
   提交代码后，当时忘记了进行tag标记。
   
   借助git log --pretty=oneline查看一下提交日志，找出补打位置对一个的commit id，然后指定commit id（前面10位即可），创建tag即可。

- 命令格式
    
    git tag -a [标签名字] -m [标签说明] [commit id] 

- 举例

    ```bash
    $ git tag -a v1.2 9fceb02
    ```

### 查看标签列表，和详情

- 查看有哪些标签

    ```bash
    $ git tag -l 
    $ git tag --list
    $ git tag -ln
    ```
   
- 查询看某个标签的信息    
   
    git show [标签名字]

- 举例

    ```bash
    # 查看有哪些标签
    $ git tag -l 
  
    # 查看标签v1.0.0的详情
    $ git show v1.0.0
    ```

### 把标签推送（分享）到远程服务器
   
   他人克隆共享仓库或拉取数据同步后，也会看到这些标签。
   
   只有master、integrate两个分支上的标签需要推送回中心库。
    
- 命令格式
    
    git push origin [标签名称]
    
- 举例

    ```bash
    $ git push origin v1.0.0
    ```

### 把所有标签推送（分享）到远程服务器

- 命令格式

    git push --tags 

- 举例

    ```bash
    $ git push --tags 
    ``` 


### 拉取tag内容（只能查看）

   先 git clone 整个仓库，然后 git checkout tag_name 就可以取得 tag 对应的代码了。
    
- 命令格式

    git checkout [标签名称] 

- 举例

    ```bash
    # clone 仓库
    $ git clone https://gitee.com/shiyunlai/HowToUseGitflow.git
  
    # 展示有哪些标签
    $ git tag -l
  
    # 获取v1.0.0标签对应的内容
    $ git checkout v1.0.0 
    ```

> 注意：这种方式只能看代码，不能修改代码，因为 tag 相当于是一个快照，是不能更改它的代码的。

### 根据tag创建一个分支（准备从tag处，开始修改内容）
   
   如果要在 tag 代码的基础上做修改，你需要一个分支。
   
   从 tag 创建一个分支 —— 得到一个新的分支，就可以修改代码了

- 命令格式
    
    git checkout -b [分支名称] [标签名称]

- 举例

    ```bash
    $ git checkout -b new_branch v1.0.0
    ```

### 删除tag
   
   删除本地仓库的tag
   
- 命令格式
    
    git tag -d [标签名称]

- 举例
    
    ```bash
    $ git tag -d v1.0.0
    ```

### 删除远程仓库上的tag
   
   在本地操作，删除已经push到远程仓库的tag。
  
- 命令格式 
    
    git push origin :refs/tags/[标签名称]

- 举例
    ```bash
    $ git push origin :refs/tag/v1.0.0
    ```
    
>想从本地和中心仓库删除同一个tag，必须分别执行删除操作（git是分布式的，相当于删除两个git库中同一个tag）。


