# 更换远程仓库地址

当远程仓库地址放生改变时，需要同步更新本地代码库对应的远程仓库地址。

如：代码从github搬迁到gitee。

>举例 

本地有keppel项目

原远程仓库地址为git@222.73.218.47:shi.yunlai/keppel.git

准备变为新仓库地址git@222.73.218.38:shi.yunlai/keppel.git

- [方式一：通过命令直接修改远程地址](#方式一：-通过命令直接修改远程地址)

- [方式二：通过命令先删除再添加远程仓库](#方式二：通过命令先删除再添加远程仓库)

- [方式三：-直接修改配置文件](#方式三：-直接修改配置文件)

- [方式四：-通过第三方git工具修改](#方式四：-通过第三方git工具修改)
 
 
## 方式一： 通过命令直接修改远程地址

- 进入工程根目录（如：keppel）
- 直接修改远程仓库地址
    
    git remote set-url [*远程仓库名称*] [*远程仓库地址*]
    
    如：
    ```bash
    $ git remote set-url origin git@222.73.218.38:shi.yunlai/keppel.git
    ```
    
- 查看当前远程仓库地址
    
    ```bash
    $ git remote -v
    ```
    
## 方式二：通过命令先删除再添加远程仓库
 
- 进入工程根目录
- 先删除远程仓库地址

    ```bash
    $ git remote rm origin
    ```
    
- 再修改为新的远程仓库地址

    ```bash
    $ git remote add origin git@222.73.218.38:shi.yunlai/keppel.git
    ```
 
## 方式三： 直接修改配置文件

- 进入工程根路径下的 .git 目录
- 修改config文件中url配置，如下图
    ![config](../img/git-config-url.png) 
- 保存退出即可 

## 方式四： 通过第三方git工具修改

百度、google
 
 
 
 