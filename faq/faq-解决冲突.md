
# 出现冲突

如：新版发布完成，将release分支合并回master、integrate时，
因master和integrate在发版期间，代码可能被改动：

- master上修复了bug
- integrate上接受了新功能提交

这些情况，可能导致代码冲突；
# 谁来解决冲突

- 优先：开发人员自己 （建议）
- 次之：库管理员

# 如何解决

把代码拉回本地，解决冲突后，再提交。

 - [step 1 更新代码](#step-1-取最新integrate、发版分支代码)
 - [step 2 创建冲突解决分支](#step-2-创建专门解决冲突的分支)
 - [step 3 合并分支](#step-3-合并发版分支)
 - [step 4 解决冲突](#step-4-手工解决冲突)
 - [step 5 提交解决后的代码](#step-5-提交解决后的代码)
 - [step 6 推回已解决冲突的分支](#step-6-将解决冲突后的分支推回远程库)
 - [step 7 提出PR](#step-7-登录远程库，通过pr，申请合并回integrate)
 - [step 8 接受PR](#step-8-库管理员接受pr)
 - [step 9 清理分支](#step-9-清理分支)


## step 1 取最新integrate、待合并分支代码

假设,待合并分支名为 release_2_v1.6.0_bike2

```bash
# 更新integrate
$ git checkout integrate 
$ git pull

# 获取发版分支 release_2_v1.6.0_bike2 
$ git checkout -b release_2_v1.6.0_bike2 origin/release_2_v1.6.0_bike2
```

## step 2 创建专门解决冲突的分支

```bash
# 创建冲突解决分支，命名建议：conflict_[to-barnch]_[from-branch]
$ git checkout integrate 
$ git checkout -b conflict_integrate_release_v1.6.0
```

## step 3 合并发版分支

待合并分支 --合并到--> integrate 

```bash
# 回integrate，合并发版分支
$ git checkout conflict_integrate_release_v1.6.0
$ git branch --no-merged #显示integrate还未合并的分支
$ git merge --no-ff release_2_v1.6.0_bike2 # 合并release_2_v1.6.0_bike2
```

> 存在冲突是，git merge 命令将提示如下冲突信息
 
Automatic merge failed; fix conflicts and then commit the result.
    
![冲突](../img/conflict.png)

## step 4 手工解决冲突 
此时，手工修复冲突，直接打开提示的文件，留下正确的代码。
也可使用工具进行冲突解决，如： IDEA、VSCode

## step 5 提交解决后的代码

解决冲突后，提交代码

``` bash
$ git commit -a -m "解决冲突：发布V1.6.0"
```

## step 6 将解决冲突后的分支推回远程库

``` bash
$ git push origin conflict_integrate_release_v1.6.0 
```

## step 7 登录远程库，提出PR

PR : conflict_integrate_release_v1.6.0 -> integrate

## step 8 库管理员接受PR

以下处理可在码云、或本地命令操作。

接受PR前,对integrate标记Tag： tag_integrate_before_v1.6.0
接受PR处理...
接受完，标记Tag：tag_integrate_after_v1.6.0

## step 9 清理分支

删除本地冲突处理分支
``` bash
$ git branch -d conflict_integrate_release_v1.6.0 
```
删除远程库冲突处理分支
``` bash
$ git push origin --delete conflict_integrate_release_v1.6.0
```

删除本地待合并分支
``` bash
$ git branch -d release_v1.6.0 
```
删除远程库待合并分支
``` bash
$ git push origin --delete release_v1.6.0
```

